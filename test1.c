#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i;
    char *addr;

    i = 0;
    while (i < 1024)
    {
        addr = (char*)malloc(1024);
        if (NULL == addr)
        {
            printf("addr null\n");
            return (-1);
        }
        addr[0] = 42;
        i++;
    }
    return (0);
}
