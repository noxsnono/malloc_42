/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projet.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/11 18:40:42 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 13:37:02 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROJET_H
# define PROJET_H

# include <sys/resource.h>
# include <sys/mman.h>
# include <errno.h>
# include <string.h>
# include <libft.h>
# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>

# define LOG_PATH		"logs/debugg.log"

# define GETPAGESIZE	sysconf(_SC_PAGE_SIZE)

# define TINY_MULTIPLE 	1
# define SMALL_MULTIPLE	2

enum	e_enum {
	SMALL,
	TINY,
	LARGE,
	FREE,
	ALLOCATED
};

typedef struct			s_data
{
	int					fd_log;
	int					i_args;
	unsigned short int	op_log;
}						t_data;

typedef struct			s_log {
	int					type;
	int					status;
	size_t				byte_used;
	void				*ptr;
}						t_log;

typedef struct			s_zone_tiny {
	void				*ptr;
	t_log				log_zone[100];
	char				*mem_zone;
	int					type;
	int					used_slot;
	struct s_zone_tiny	*next;
}						t_zone_tiny;

typedef struct			s_zone_small {
	void				*ptr;
	t_log				log_zone[100];
	char				*mem_zone;
	int					type;
	int					used_slot;
	struct s_zone_small	*next;
}						t_zone_small;

typedef struct			s_zone_large {
	void				*ptr;
	t_log				log_zone[100];
	char				*mem_zone;
	int					type;
	int					used_slot;
	struct s_zone_large	*next;
}						t_zone_large;

int						error(char const *str);
int						init_start(t_data **d, int ac, char **av);
int						open_log(t_data *d);
t_data					*get_data(void);
void					w_log(char const *str);

size_t					get_tiny_size(void);
size_t					get_small_size(void);
size_t					get_large_size(void);
t_zone_tiny				*single_get_tiny_ptr(void);
t_zone_small			*single_get_small_ptr(void);
t_zone_large			*single_get_large_ptr(void);
int						single_debugging(void);

void					*ft_malloc(size_t size);
void					ft_free(void *ptr);
void					ft_free_2(size_t *free_success, void *ptr);
void					ft_free_3(size_t *free_success, void *ptr);
void					ft_free_4(size_t *free_success, void *ptr);
void					ft_free_5(t_zone_large *tmp_large, int i, \
							size_t *free_success);
void					*ft_realloc(void *ptr, size_t size);
void					ft_show_alloc_mem(void);
int						malloc_firstcall(void);
int						tiny_list(int type);
t_zone_tiny				*tiny_list_2(int type);
void					tiny_list_3(t_zone_tiny	*tmp, int *total);
size_t					get_allocated_size_from_ptr(void *ptr);
size_t					get_allocated_size_from_ptr_tiny(void *ptr, \
							size_t *find);
size_t					get_allocated_size_from_ptr_small(void *ptr, \
							size_t *find);
size_t					get_allocated_size_from_ptr_large(void *ptr, \
							size_t *find);
void					*ft_copy_mem(void *dst, void const *src, size_t size);
int						check_debugg_on(void);

t_zone_tiny				*tiny_create_zone(int type);
void					tiny_pushback_zone(t_zone_tiny *list, t_zone_tiny *new);
int						tiny_split_zone_tiny(t_zone_tiny *zone, int type);
t_zone_tiny				*tiny_get_slot_memory(size_t size);
t_zone_tiny				*tiny_slot_from_zone(t_zone_tiny *list_tiny_zone, \
							size_t size);

t_zone_small			*small_create_zone(int type);
void					small_pushback_zone(t_zone_small *list, \
							t_zone_small *new);
int						small_split_zone_small(t_zone_small *zone, int type);
t_zone_small			*small_get_slot_memory(size_t size);
t_zone_small			*small_slot_from_zone(t_zone_small *list_small_zone, \
							size_t size);

t_zone_large			*large_create_zone(int type);
void					large_pushback_zone(t_zone_large *list, \
							t_zone_large *new);
int						large_split_zone_large(t_zone_large *zone, int type);
t_zone_large			*large_get_slot_memory(size_t size);
t_zone_large			*large_slot_from_zone(t_zone_large *list_large_zone, \
							size_t size);

extern void				*malloc(size_t size);
extern void				free(void *ptr);
extern void				*realloc(void *ptr, size_t size);
extern void				show_alloc_mem(void);

#endif
