/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 14:46:07 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/13 15:13:53 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

void	*ft_realloc(void *ptr, size_t size)
{
	void	*new_memory;
	size_t	old_size;

	old_size = 0;
	if (NULL == ptr)
		return (ft_malloc(size));
	else if (NULL != ptr && 0 == size)
	{
		ft_free(ptr);
		return (NULL);
	}
	else if (NULL != ptr)
	{
		old_size = get_allocated_size_from_ptr(ptr);
		if (old_size == 0)
			return (NULL);
		new_memory = ft_malloc(size);
		if (NULL == new_memory)
			return (NULL);
		new_memory = ft_memcpy(new_memory, ptr, sizeof(unsigned char) \
						* old_size);
		ft_free(ptr);
		return (new_memory);
	}
	return (NULL);
}

size_t	get_allocated_size_from_ptr(void *ptr)
{
	size_t			find;
	int				result;

	find = 0;
	result = 0;
	if (NULL != ptr)
	{
		result = get_allocated_size_from_ptr_tiny(ptr, &find);
		if (0 != find)
			return (result);
		result = get_allocated_size_from_ptr_small(ptr, &find);
		if (0 != find)
			return (result);
		result = get_allocated_size_from_ptr_large(ptr, &find);
		if (0 != find)
			return (result);
		return (0);
	}
	return (0);
}

size_t	get_allocated_size_from_ptr_tiny(void *ptr, size_t *find)
{
	t_zone_tiny		*tmp_tiny;
	int				i;

	tmp_tiny = single_get_tiny_ptr();
	while (NULL != tmp_tiny)
	{
		i = 0;
		while (i < 100)
		{
			if (ptr == tmp_tiny->log_zone[i].ptr)
			{
				*find = 1;
				return (tmp_tiny->log_zone[i].byte_used);
			}
			i++;
		}
		tmp_tiny = tmp_tiny->next;
	}
	return (0);
}

size_t	get_allocated_size_from_ptr_small(void *ptr, size_t *find)
{
	t_zone_small	*tmp_small;
	int				i;

	tmp_small = single_get_small_ptr();
	while (NULL != tmp_small)
	{
		i = 0;
		while (i < 100)
		{
			if (ptr == tmp_small->log_zone[i].ptr)
			{
				*find = 1;
				return (tmp_small->log_zone[i].byte_used);
			}
			i++;
		}
		tmp_small = tmp_small->next;
	}
	return (0);
}

size_t	get_allocated_size_from_ptr_large(void *ptr, size_t *find)
{
	t_zone_large	*tmp_large;
	int				i;

	tmp_large = single_get_large_ptr();
	while (NULL != tmp_large)
	{
		i = 0;
		while (i < 100)
		{
			if (ptr == tmp_large->log_zone[i].ptr)
			{
				*find = 1;
				return (tmp_large->log_zone[i].byte_used);
			}
			i++;
		}
		tmp_large = tmp_large->next;
	}
	return (0);
}
