/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_t_zone_tiny.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/01 23:03:52 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/12 18:52:19 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

t_zone_tiny	*tiny_create_zone(int type)
{
	t_zone_tiny *tmp;

	tmp = (t_zone_tiny *)mmap(0, sizeof(t_log), PROT_READ | PROT_WRITE, \
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (MAP_FAILED == tmp)
	{
		ft_putendl(strerror(errno));
		return (NULL);
	}
	tmp->type = type;
	tmp->used_slot = 0;
	tmp->next = NULL;
	if (tiny_split_zone_tiny(tmp, TINY) < 0)
		return (NULL);
	return (tmp);
}

void		tiny_pushback_zone(t_zone_tiny *list, t_zone_tiny *new)
{
	t_zone_tiny *tmp;

	tmp = list;
	while (NULL != tmp->next)
		tmp = tmp->next;
	tmp->next = new;
}

int			tiny_split_zone_tiny(t_zone_tiny *zone, int type)
{
	int	i;

	zone->mem_zone = (char *)mmap(0, (get_tiny_size() * 100), \
			PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (MAP_FAILED == zone->mem_zone)
		return (-1);
	zone->ptr = zone;
	i = 0;
	while (i < 100)
	{
		zone->log_zone[i].type = type;
		zone->log_zone[i].status = FREE;
		zone->log_zone[i].byte_used = 0;
		zone->log_zone[i].ptr = &zone->mem_zone[i * get_tiny_size()];
		i++;
	}
	return (0);
}

t_zone_tiny	*tiny_get_slot_memory(size_t size)
{
	t_zone_tiny *tmp;
	t_zone_tiny *new;
	void		*new_memory;

	tmp = single_get_tiny_ptr();
	while (tmp != NULL)
	{
		if (NULL != tmp && tmp->used_slot < 100)
			return (tiny_slot_from_zone(tmp, size));
		tmp = tmp->next;
	}
	new = tiny_create_zone(TINY);
	if (NULL == new)
		return (NULL);
	tmp = single_get_tiny_ptr();
	tiny_pushback_zone(tmp, new);
	new_memory = tiny_slot_from_zone(new, size);
	if (NULL != new_memory)
		return (new_memory);
	return (NULL);
}

t_zone_tiny	*tiny_slot_from_zone(t_zone_tiny *list_tiny_zone, size_t size)
{
	int			index;
	static void	*last_ptr = NULL;

	index = 0;
	if (list_tiny_zone->used_slot < 100)
	{
		while (index < 100)
		{
			if (FREE == list_tiny_zone->log_zone[index].status)
			{
				last_ptr = (void *)list_tiny_zone->log_zone[index].ptr;
				list_tiny_zone->log_zone[index].status = ALLOCATED;
				list_tiny_zone->log_zone[index].byte_used = size;
				list_tiny_zone->used_slot += 1;
				return (void *)list_tiny_zone->log_zone[index].ptr;
			}
			index++;
		}
	}
	return (NULL);
}
