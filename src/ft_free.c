/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 14:15:14 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/13 15:28:03 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

void	ft_free(void *ptr)
{
	t_zone_tiny		*tmp_tiny;
	t_zone_small	*tmp_small;
	t_zone_large	*tmp_large;
	size_t			free_success;

	tmp_tiny = NULL;
	tmp_small = NULL;
	tmp_large = NULL;
	free_success = 0;
	if (NULL != ptr)
	{
		tmp_tiny = single_get_tiny_ptr();
		ft_free_2(&free_success, ptr);
		if (0 == free_success)
			ft_free_3(&free_success, ptr);
		if (0 == free_success)
			ft_free_4(&free_success, ptr);
	}
}

void	ft_free_2(size_t *free_success, void *ptr)
{
	t_zone_tiny		*tmp_tiny;
	int				i;

	tmp_tiny = NULL;
	tmp_tiny = single_get_tiny_ptr();
	while (NULL != tmp_tiny)
	{
		i = 0;
		while (i < 100)
		{
			if (ALLOCATED == tmp_tiny->log_zone[i].status \
				&& ptr == tmp_tiny->log_zone[i].ptr)
			{
				*free_success = 1;
				tmp_tiny->log_zone[i].status = FREE;
				tmp_tiny->log_zone[i].byte_used = 0;
				(tmp_tiny->used_slot)--;
			}
			i++;
		}
		tmp_tiny = tmp_tiny->next;
	}
}

void	ft_free_3(size_t *free_success, void *ptr)
{
	t_zone_small	*tmp_small;
	int				i;

	tmp_small = NULL;
	{
		tmp_small = single_get_small_ptr();
		while (NULL != tmp_small)
		{
			i = 0;
			while (i < 100)
			{
				if (ALLOCATED == tmp_small->log_zone[i].status \
					&& ptr == tmp_small->log_zone[i].ptr)
				{
					*free_success = 1;
					tmp_small->log_zone[i].status = FREE;
					tmp_small->log_zone[i].byte_used = 0;
					(tmp_small->used_slot)--;
				}
				i++;
			}
			tmp_small = tmp_small->next;
		}
	}
}

void	ft_free_4(size_t *free_success, void *ptr)
{
	t_zone_large	*tmp_large;
	int				i;

	tmp_large = single_get_large_ptr();
	while (NULL != tmp_large)
	{
		i = 0;
		while (i < 100)
		{
			if (ALLOCATED == tmp_large->log_zone[i].status \
					&& ptr == tmp_large->log_zone[i].ptr)
				ft_free_5(tmp_large, i, free_success);
			i++;
		}
		tmp_large = tmp_large->next;
	}
}

void	ft_free_5(t_zone_large *tmp_large, int i, size_t *free_success)
{
	if (munmap(tmp_large->log_zone[i].ptr,
		tmp_large->log_zone[i].byte_used) < 0)
	{
		ft_putendl(strerror(errno));
		error("free:: LARGE munmap failed");
	}
	else
	{
		*free_success = 1;
		tmp_large->log_zone[i].status = FREE;
		tmp_large->log_zone[i].byte_used = 0;
		(tmp_large->used_slot)--;
	}
}
