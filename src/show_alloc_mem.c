/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 11:04:30 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 13:35:52 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

void		ft_show_alloc_mem(void)
{
	int total;

	total = 0;
	total += tiny_list(TINY);
	total += tiny_list(SMALL);
	total += tiny_list(LARGE);
	ft_putstr("Total : ");
	ft_putnbr(total);
	ft_putendl(" octets");
}

int			tiny_list(int type)
{
	int			total;
	t_zone_tiny	*tmp;

	total = 0;
	tmp = tiny_list_2(type);
	while (NULL != tmp)
	{
		if (TINY == type)
			ft_putstr("TINY: 0x");
		else if (SMALL == type)
			ft_putstr("SMALL: 0x");
		else if (LARGE == type)
			ft_putstr("LARGE: 0x");
		ft_putnbrhexa((unsigned long)tmp->ptr);
		ft_putchar('\n');
		tiny_list_3(tmp, &total);
		tmp = tmp->next;
	}
	return (total);
}

t_zone_tiny	*tiny_list_2(int type)
{
	t_zone_tiny	*tmp;

	tmp = NULL;
	if (TINY == type)
		tmp = single_get_tiny_ptr();
	else if (SMALL == type)
		tmp = (t_zone_tiny *)single_get_small_ptr();
	else if (LARGE == type)
		tmp = (t_zone_tiny *)single_get_large_ptr();
	return (tmp);
}

void		tiny_list_3(t_zone_tiny *tmp, int *total)
{
	int			i;
	t_log		*start;
	void		*end;

	i = 0;
	while (i < 100)
	{
		if (tmp && tmp->log_zone[i].status == ALLOCATED)
		{
			*total += tmp->log_zone[i].byte_used;
			start = tmp->log_zone[i].ptr;
			end = &start[tmp->log_zone[i].byte_used];
			ft_putstr("0x");
			ft_putnbrhexa((unsigned long)start);
			ft_putstr(" - ");
			ft_putstr("0x");
			ft_putnbrhexa((unsigned long)end);
			ft_putstr(" : ");
			ft_putnbr(tmp->log_zone[i].byte_used);
			ft_putendl(" octets");
		}
		i++;
	}
}
