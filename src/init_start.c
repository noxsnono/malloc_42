/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_start.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/01 22:21:46 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/02 23:16:09 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

int	init_start(t_data **d, int ac, char **av)
{
	(void)ac;
	(void)av;
	*d = get_data();
	if (NULL == d)
		return (error("init_start:: t_data init is NULL, malloc failed"));
	if (open_log(*d) < 0)
		return (error("init_start:: Open_log error"));
	return (0);
}
