/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_get_size.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/01 22:21:27 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 13:51:57 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

size_t	get_tiny_size(void)
{
	return ((GETPAGESIZE / 4));
}

size_t	get_small_size(void)
{
	return ((get_tiny_size() * 8));
}

size_t	get_large_size(void)
{
	return (get_small_size() + 1);
}
