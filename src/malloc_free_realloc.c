/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_free_realloc.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:26:33 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 13:37:01 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

extern void	*malloc(size_t size)
{
	static int	called = 0;

	called++;
	if (1 == single_debugging())
	{
		ft_putnbr(called);
		ft_putendl(" Call Malloc");
	}
	return (ft_malloc(size));
}

extern void	free(void *ptr)
{
	static int	called = 0;

	called++;
	if (1 == single_debugging())
	{
		ft_putnbr(called);
		ft_putendl(" Call Free ");
	}
	ft_free(ptr);
}

extern void	*realloc(void *ptr, size_t size)
{
	static int	called = 0;

	called++;
	if (1 == single_debugging())
	{
		ft_putnbr(called);
		ft_putendl(" Call Realloc");
	}
	return (ft_realloc(ptr, size));
}

extern void	show_alloc_mem(void)
{
	ft_show_alloc_mem();
}
