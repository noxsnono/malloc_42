/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 22:51:08 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 12:55:27 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

void	*ft_malloc(size_t size)
{
	if (0 == malloc_firstcall())
	{
		single_debugging();
		if (NULL == single_get_tiny_ptr() || NULL == single_get_small_ptr() \
			|| NULL == single_get_large_ptr())
			return (NULL);
	}
	if (size > 0 && size <= get_tiny_size())
		return (tiny_get_slot_memory(size));
	else if (size > get_tiny_size() && size <= get_small_size())
		return (small_get_slot_memory(size));
	else if (size > get_large_size())
		return (large_get_slot_memory(size));
	return (NULL);
}

int		malloc_firstcall(void)
{
	static int	firstcall = 0;

	if (firstcall == 0)
	{
		firstcall = 1;
		return (0);
	}
	return (1);
}
