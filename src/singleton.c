/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   singleton.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 10:14:38 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 13:50:09 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

t_zone_tiny		*single_get_tiny_ptr(void)
{
	static t_zone_tiny	*list_tiny_zone = NULL;

	if (NULL == list_tiny_zone)
	{
		list_tiny_zone = tiny_create_zone(TINY);
		if (NULL == list_tiny_zone)
			return (NULL);
	}
	return (list_tiny_zone);
}

t_zone_small	*single_get_small_ptr(void)
{
	static t_zone_small	*list_small_zone = NULL;

	if (NULL == list_small_zone)
	{
		list_small_zone = small_create_zone(TINY);
		if (NULL == list_small_zone)
			return (NULL);
	}
	return (list_small_zone);
}

t_zone_large	*single_get_large_ptr(void)
{
	static t_zone_large	*list_large_zone = NULL;

	if (NULL == list_large_zone)
	{
		list_large_zone = large_create_zone(LARGE);
		if (NULL == list_large_zone)
			return (NULL);
	}
	return (list_large_zone);
}

int				single_debugging(void)
{
	static int	debugg = 0;
	static int	firstcall = 0;

	if (0 == firstcall)
	{
		debugg = check_debugg_on();
		firstcall = 1;
	}
	return (debugg);
}

int				check_debugg_on(void)
{
	char		*tmp;
	int			activated;

	activated = 0;
	tmp = NULL;
	tmp = getenv("FT_MALLOC_DEBUGG_CALL");
	if (NULL != tmp && 1 == ft_strlen(tmp) && '1' == tmp[0])
		activated = 1;
	tmp = NULL;
	tmp = getenv("FT_MALLOC_DEBUGG_SIZE");
	if (NULL != tmp && 1 == ft_strlen(tmp) && '1' == tmp[0])
	{
		ft_putstr("getpagesize = ");
		ft_putnbr(sysconf(_SC_PAGE_SIZE));
		ft_putstr(" | TINY() <= ");
		ft_putnbr(get_tiny_size());
		ft_putstr(" | SMALL() <= ");
		ft_putnbr(get_small_size());
		ft_putstr(" | LARGE() >= ");
		ft_putnbr(get_large_size());
		ft_putchar('\n');
	}
	return (activated);
}
