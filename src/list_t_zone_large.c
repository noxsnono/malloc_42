/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_t_zone_large.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 18:30:05 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/14 13:13:06 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

t_zone_large	*large_create_zone(int type)
{
	t_zone_large *tmp;

	tmp = (t_zone_large *)mmap(0, sizeof(t_log), PROT_READ | PROT_WRITE, \
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (MAP_FAILED == tmp)
	{
		ft_putendl(strerror(errno));
		return (NULL);
	}
	tmp->type = type;
	tmp->used_slot = 0;
	tmp->next = NULL;
	if (large_split_zone_large(tmp, TINY) < 0)
		return (NULL);
	return (tmp);
}

void			large_pushback_zone(t_zone_large *list, t_zone_large *new)
{
	t_zone_large *tmp;

	tmp = list;
	while (NULL != tmp->next)
		tmp = tmp->next;
	tmp->next = new;
}

int				large_split_zone_large(t_zone_large *zone, int type)
{
	int	i;

	i = 0;
	zone->ptr = zone;
	while (i < 100)
	{
		zone->log_zone[i].type = type;
		zone->log_zone[i].status = FREE;
		zone->log_zone[i].ptr = NULL;
		i++;
	}
	return (0);
}

t_zone_large	*large_get_slot_memory(size_t size)
{
	t_zone_large	*tmp;
	t_zone_large	*new;
	void			*new_memory;

	tmp = single_get_large_ptr();
	while (tmp != NULL)
	{
		if (NULL != tmp && tmp->used_slot < 100)
			return (large_slot_from_zone(tmp, size));
		tmp = tmp->next;
	}
	new = large_create_zone(TINY);
	if (NULL == new)
		return (NULL);
	tmp = single_get_large_ptr();
	large_pushback_zone(tmp, new);
	new_memory = large_slot_from_zone(new, size);
	if (NULL != new_memory)
		return (new_memory);
	return (NULL);
}

t_zone_large	*large_slot_from_zone(t_zone_large *list_large_zone, \
					size_t size)
{
	int			index;

	index = 0;
	if (list_large_zone->used_slot < 100)
	{
		while (index < 100)
		{
			if (FREE == list_large_zone->log_zone[index].status)
			{
				list_large_zone->log_zone[index].status = ALLOCATED;
				list_large_zone->log_zone[index].byte_used = size;
				list_large_zone->used_slot += 1;
				list_large_zone->log_zone[index].ptr = mmap(0, size, \
						PROT_READ | PROT_WRITE, \
						MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
				if (NULL == list_large_zone->log_zone[index].ptr)
					return (NULL);
				return (list_large_zone->log_zone[index].ptr);
			}
			index++;
		}
	}
	return (NULL);
}
