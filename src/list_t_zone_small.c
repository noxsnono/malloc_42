/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_t_zone_small.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 18:12:30 by jmoiroux          #+#    #+#             */
/*   Updated: 2016/01/12 18:54:41 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <projet.h>

t_zone_small	*small_create_zone(int type)
{
	t_zone_small *tmp;

	tmp = (t_zone_small *)mmap(0, sizeof(t_log), PROT_READ | PROT_WRITE, \
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (MAP_FAILED == tmp)
	{
		ft_putendl(strerror(errno));
		return (NULL);
	}
	tmp->type = type;
	tmp->used_slot = 0;
	tmp->next = NULL;
	if (small_split_zone_small(tmp, TINY) < 0)
		return (NULL);
	return (tmp);
}

void			small_pushback_zone(t_zone_small *list, t_zone_small *new)
{
	t_zone_small *tmp;

	tmp = list;
	while (NULL != tmp->next)
		tmp = tmp->next;
	tmp->next = new;
}

int				small_split_zone_small(t_zone_small *zone, int type)
{
	int	i;

	zone->mem_zone = (char *)mmap(0, (get_small_size() * 100), \
			PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (MAP_FAILED == zone->mem_zone)
		return (-1);
	zone->ptr = zone;
	i = 0;
	while (i < 100)
	{
		zone->log_zone[i].type = type;
		zone->log_zone[i].status = FREE;
		zone->log_zone[i].byte_used = 0;
		zone->log_zone[i].ptr = &zone->mem_zone[i * get_small_size()];
		i++;
	}
	return (0);
}

t_zone_small	*small_get_slot_memory(size_t size)
{
	t_zone_small	*tmp;
	t_zone_small	*new;
	void			*new_memory;

	tmp = single_get_small_ptr();
	while (tmp != NULL)
	{
		if (NULL != tmp && tmp->used_slot < 100)
			return (small_slot_from_zone(tmp, size));
		tmp = tmp->next;
	}
	new = small_create_zone(TINY);
	if (NULL == new)
		return (NULL);
	tmp = single_get_small_ptr();
	small_pushback_zone(tmp, new);
	new_memory = small_slot_from_zone(new, size);
	if (NULL != new_memory)
		return (new_memory);
	return (NULL);
}

t_zone_small	*small_slot_from_zone(t_zone_small *list_small_zone, \
					size_t size)
{
	int			index;
	static int	called = 1;
	static void	*last_ptr = NULL;

	index = 0;
	if (list_small_zone->used_slot < 100)
	{
		while (index < 100)
		{
			if (FREE == list_small_zone->log_zone[index].status)
			{
				if (last_ptr != (void *)list_small_zone->log_zone[index].ptr)
					called++;
				last_ptr = (void *)list_small_zone->log_zone[index].ptr;
				list_small_zone->log_zone[index].status = ALLOCATED;
				list_small_zone->log_zone[index].byte_used = size;
				list_small_zone->used_slot += 1;
				return ((void *)list_small_zone->log_zone[index].ptr);
			}
			index++;
		}
	}
	return (NULL);
}
