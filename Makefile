HOSTTYPE := $(HOSTTYPE)
ifndef HOSTTYPE
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

CC = $(GCC)

HEADER = includes/projet.h
INCLUDE = -I libft/includes/ -I includes/

NAME = libft_malloc_$(HOSTTYPE).so
MALLOC_LIB = libft_malloc.so
NAME_TEST = test_malloc

SRCS_SHARED = src/malloc_free_realloc.c
SRCS_SHARED_O = src/malloc_free_realloc.o

SRCS =	src/basic_functions.c \
		src/init_start.c \
		src/malloc_get_size.c \
		src/list_t_zone_tiny.c \
		src/list_t_zone_small.c \
		src/list_t_zone_large.c \
		src/ft_malloc.c \
		src/singleton.c \
		src/show_alloc_mem.c \
		src/ft_free.c \
		src/ft_realloc.c

SRCS_TEST =	src/basic_functions.c \
			src/init_start.c \
			src/malloc_get_size.c \
			src/list_t_zone_tiny.c \
			src/list_t_zone_small.c \
			src/list_t_zone_large.c \
			src/ft_malloc.c \
			src/singleton.c \
			src/show_alloc_mem.c \
			src/ft_free.c \
			src/ft_realloc.c \
			test/main.c

GCC = gcc
CLANG = clang
CFLAGS = -Wall -Werror -Wextra -std=gnu89 -pedantic -o3
LIB = libft/libft.a
OBJS = $(SRCS:.c=.o)
OBJS_TEST = $(SRCS_TEST:.c=.o)
OBJS_SHARED_O = $(OBJS_SHARED:.c=.o)

all: $(MALLOC_LIB)

$(LIB):
	make -C libft

%.o: %.c $(HEADER)
	@$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

$(NAME): $(LIB) $(OBJS) $(HEADER)
	$(CC) $(CFLAGS) -fpic -c $(INCLUDE) src/malloc_free_realloc.c -o src/malloc_free_realloc.o
	$(CC) $(CFLAGS) -shared $(INCLUDE) $(OBJS) $(LIB) src/malloc_free_realloc.c -o $(NAME)

$(MALLOC_LIB) : $(NAME)
	ln -s $(NAME) $(MALLOC_LIB)

so1: $(LIB) $(OBJS)
	$(CC) $(CFLAGS) -fpic -c $(INCLUDE) src/malloc_free_realloc.c \
	-o src/malloc_free_realloc.o

so2: $(LIB) $(OBJS)
	$(CC) $(CFLAGS) -shared $(INCLUDE) $(OBJS) $(LIB) src/malloc_free_realloc.o \
	-o shared_so.so

test: $(NAME_TEST)

correction:
	gcc -o test1 test1.c
	gcc -o test2 test2.c
	gcc -o test3 test3.c
	gcc -o test3_modif test3_modif.c
	gcc -o test4 test4.c
	gcc -o test5 test5.c -L. -lft_malloc

clean_correction:
	rm test1 test2 test3 test3_modif test4 test5

$(NAME_TEST): $(LIB) $(OBJS_TEST) $(HEADER)
	$(CC) $(CFLAGS) $(OBJS_TEST) $(INCLUDE) -o $(NAME_TEST) $(LIB)

clean:
	make clean -C libft
	rm -rf $(OBJS_TEST)
	rm -rf $(SRCS_SHARED_O)

fclean: clean
	make fclean -C libft
	rm -rf $(NAME)
	rm -rf $(MALLOC_LIB)
	rm -rf $(NAME_TEST)

re: fclean all

.PHONY: all clean fclean re
